# GNU/Linux Hardening

## Wiki

Deployed on [hardening.xanhacks.xyz](https://hardening.xanhacks.xyz/).

- Software : [Hugo](https://gohugo.io/)
- Theme : [hugo-geekdoc](https://themes.gohugo.io/themes/hugo-geekdoc/)

## ANSSI Guide

- [Recommandations de sécurité relatives à un système GNU/Linux - v2. 0](https://www.ssi.gouv.fr/uploads/2019/02/fr_np_linux_configuration-v2.0.pdf)

