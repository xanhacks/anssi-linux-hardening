### Partitions options list

Here are the recommendations related to the options of the partitions of a linux system:

| Mount point   | Mount options       | Description   |
| ------------- |:-------------------:|:--------------|
| /             | no option           | Root partition, contains the rest of the tree structure |
| /boot         | nosuid,nodev,noexec | Contains the kernel and boot loader. No access needed after the boot finished (except update) |
| /opt          | nosuid,nodev        | Additional packages to the system. Read-only mounting if not used. |
| /tmp          | nosuid,nodev,noexec | Temporary files. Must contain only non executable items. Cleaned after reboot or preferably of type `tmpfs` |
| /srv          | nosuid,nodev        | Contains files served by a service like Web, FTP... |
| /home         | nosuid,nodev,noexec | Contains `HOME` of users. | 
| /proc         | hidepid=2           | Contains process and system information. |
| /usr          | nodev               | Contains most of the utilities and system files. |
| /var          | nosuid,nodev,noexec | Partition containing files that vary during the life of the system (mails, PID files, service databases). service databases) |
| /var/log      | nosuid,nodev,noexec | Contains system logs. |
| /var/tmp      | nosuid,nodev,noexec | Temporary files kept after shutdown. |

## R29 - Restrict access to the /boot folder

When possible, it is recommended not to automatically mount the /boot partition. In any case, access to the /boot folder should be allowed only for the root user.

```bash
root@debian:/root# sudo mount -o remount,nosuid,nodev,noexec /boot
```

In order for the changes to persist after a reboot, you must apply the changes in the `/etc/fstab` file.
