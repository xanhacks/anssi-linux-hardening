Unix and Unix-like operating systems, especially GNU/Linux, have a separation of privileges that is based mainly on the notion of two-level access accounts: the "classic" user accounts and the system administrator account (commonly called root). and the system administrator account (commonly called root). If this separation lacks finesse, especially today, where a user has access to system primitives (while being more and more exposed to attacks: theft of accounts and identifiers of accounts and identifiers, information leakage...), the in-depth analysis of an expert is often analysis is often required to highlight residual vulnerabilities in order to minimize their consequences as much as possible the consequences.

### Users Accounts

#### R30 - Disable unused user accounts

We need to disable all unused users, in our case we just need 2 users.

list of used users : { `debian`,`root` }

#### R31 - Use strong passwords

Some exemples :

```
root:JwzvZR8wvvOtcE59dSnO
debian:RU7Te5VPVoQIALQsoOew
```

#### R32 - Expire local user sessions

We need to setup a bash timeout to automaticly logout an idle user. We can do it by adding `TMOUT=number_of_seconds`
in `/etc/profile`.

example : `TMOUT=300` (5 minutes) :

```bash
root@debian:/home/debian# vi /etc/profile
[...]
TMOUT=300
[...]
```

### Admin Accounts

list of administrators : { `root` }

#### R33 : Ensure accountability of administration actions

The system administration account (or `root`) must be disabled. Actions requiring privileged rights will have to rely 
on sudo which will log the commands performed.

To lock the `root` account : 
```bash
usermod -L -e 1 root
```


To disable the login shell of the `root` account :
```bash
usermod -s /bin/false root
```

### Services Accounts

list of services accounts : { 
`daemon` 
`bin` 
`sys` 
`games` 
`man` 
`lp` 
`mail` 
`news` 
`uucp` 
`proxy` 
`www-data` 
`backup`
`list`
`irc`
`gnats`
`systemd-network`
`systemd-resolve`
`messagebus`
`systemd-timesync`
`systemd-coredump`
`sshd`
`Debian-exim`
`clamav`
`bind`
`arpwatch`
}


Required Users :

- `root` : The root User is used as a super-admin
- `bin` : The bin User is included for compatibility with legacy applications.
New applications should no longer use the bin User
- `daemon` : The daemon user was used as an unprivileged user for daemons to execute under in 
order to limit their access to the system. 

Optional Users :

- `adm` : Administrative special privileges
- `lp` : Printer special privileges
- `sync` : Login to sync the system
- `shutdown` : Login to shutdown the system
- `halt` : Login to halt the system
- `mail` : Mail special privileges
- `news` : News special privileges
- `uucp` : UUCP special privileges
- `operator` : Operator special privileges
- `man` : Man special privileges
- `nobody` : Used by NFS


#### R34 - Disable unused account of services

we can disable all users listed above as Optional. For others we can verify the destination shell is `/sbin/nologin`

```bash
root@debian:/home/debian# /usr/sbin/userdel adm
root@debian:/home/debian# /usr/sbin/userdel lp
root@debian:/home/debian# /usr/sbin/userdel sync
root@debian:/home/debian# /usr/sbin/userdel shutdown
root@debian:/home/debian# /usr/sbin/userdel halt
root@debian:/home/debian# /usr/sbin/userdel mail
root@debian:/home/debian# /usr/sbin/userdel news
root@debian:/home/debian# /usr/sbin/userdel uucp
root@debian:/home/debian# /usr/sbin/userdel games
root@debian:/home/debian# /usr/sbin/userdel operator
root@debian:/home/debian# /usr/sbin/userdel man
root@debian:/home/debian# /usr/sbin/userdel nobody
```

```bash
root@debian:/home/debian# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
systemd-network:x:101:102:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:102:103:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:103:109::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:104:110:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
debian:x:1000:1000:debian,,,:/home/debian:/bin/bash
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
sshd:x:105:65534::/run/sshd:/usr/sbin/nologin
Debian-exim:x:106:112::/var/spool/exim4:/usr/sbin/nologin
clamav:x:107:113::/var/lib/clamav:/bin/false
bind:x:108:114::/var/cache/bind:/usr/sbin/nologin
arpwatch:x:109:115:ARP Watcher,,,:/var/lib/arpwatch:/bin/sh
```

```bash
root@debian:/home/debian# /usr/sbin/usermod -s /usr/sbin/nologin clamav
root@debian:/home/debian# /usr/sbin/usermod -s /usr/sbin/nologin arpwatch
```

#### R35 - Use unique and exclusive service accounts

Each business service (nginx, php, mysql, ...) added by the administrator must have its own dedicated system account.