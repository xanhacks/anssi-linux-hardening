AppArmor allows you to block the use of POSIX capabilities, as well as and to restrict network usage (network directive).

### R45 - Enable security profiles of AppArmor

Any AppArmor security profile on the system must be set to enforce mode by default when the distribution supports it and does not operate security module other than AppArmor.

To check out the status of AppArmor, you can use the following command :

```
root@debian:~# aa-status
apparmor module is loaded.
```
