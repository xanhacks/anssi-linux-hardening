---
title: System configuration
---

> This chapter deals with recommendations for the first installation of the system. Each operating system and GNU/Linux distribution takes its own path during this step. However, a few configuration points will apply almost universally.

{{< toc-tree >}}