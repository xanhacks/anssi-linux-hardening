Access control consists of ensuring that an access account has minimal rights to access a given resource. Although access control allows partitioning to be performed, the approach chosen is generally different from that adopted by virtualization mechanisms. Indeed, access control often leaves the reference to a system object visible to the application and returns an error in case of insufficient privilege to access it, whereas a system based on virtualization partitions the application by the absence of reference to this object (pointer, access path...).

#### R36 - Change default umask

You can change the default value of umask to 0027 inside the `/etc/profile` file.

- New **file** will have read/write access to the owner and only the read permission to the group.
- New **directory** will have read/write/access to the owner and only read/acess to the group.

```
root@debian:~# ls -l
total 8
drwxr-x---. 2 root root 4096 24 nov.  11:08 directory
-rw-r-----. 1 root root    0 24 nov.  11:08 file
```

#### R38 - Add custom group for sudo

The default sudo binary permission allows anyone to execute it.

```
root@debian:~# ls -l /usr/bin/sudo
-rwsr-xr-x. 1 root root 182600 27 févr.  2021 /usr/bin/sudo
```

You can create a new custom group, for example `sudogrp`, and restrict the use of sudo to this group.

```
root@debian:~# groupadd sudogrp
root@debian:~# chown root:sudogrp /usr/bin/sudo
root@debian:~# chmod u=rwxs,g=rx,o= /usr/bin/sudo
root@debian:~# ls -l /usr/bin/sudo
-rwsr-x---. 1 root sudogrp 182600 27 févr.  2021 /usr/bin/sudo
```

#### R39 - Modify sudo configuration

> This section includes recommendations R40 and R41.

You can change the default configuration of sudo to be less permissive :

- **noexec** : prevent a dynamically-linked executable from running further commands itself
- **requiretty** : require the user to have a login tty
- **use_pty** : use a pseudo-tty when a command is executed
- **umask=0077** : force umask to a more restrictive mask
- **ignore_dot** : ignore the current directory (".") in $PATH
- **env_reset** : reset environment variables

The sudo configuration is located at `/etc/sudoers`. An example will look like this :

```
Defaults noexec, requiretty, use_pty, umask =0027  
Defaults ignore_dot, env_reset
```

You can add a specific command on sudo for a user, with the following line :

```
%sudogrp ALL=EXEC: /usr/bin/whoami
```

Demonstration :

```
debian@debian:~$ sudo /usr/bin/whoami
[sudo] Mot de passe de debian :
root
debian@debian:~$ sudo id
Désolé, l'utilisateur debian n'est pas autorisé à exécuter « /usr/bin/id » en tant que root sur debian.
```

#### R42 - Ban negations in sudo specifications

Please, do not use blacklist like `user ALL = ALL,!/bin/sh`. However, you can use whitelists instead.

#### R43 - Specify the arguments in the sudo specifications

For example, you can allow `apt update` and disable other arguments of `apt` like the command `apt upgrade`.

```bash
debian@debian:~$ sudo apt update
[sudo] Mot de passe de debian :
Atteint :1 http://deb.debian.org/debian bullseye InRelease
[...]
debian@debian:~$ sudo apt upgrade
Désolé, l'utilisateur debian n'est pas autorisé à exécuter « /usr/bin/apt upgrade » en tant que root sur debian
```

Configuration used : `%sudogrp ALL=EXEC: /usr/bin/apt update`.

#### R44 - Editing files securely with sudo

To edit the sudo configuration, you need to use the utility `sudoedit`.

Example : `sudoedit /etc/sudoers`