The installation of the packages is the crucial step that will determine all the files that will be present on the system and the services rendered by it. The installed packages must be maintained over time.

#### R58 - Install only the strictly necessary packages

The choice of packages should lead to an installation that is as minimal as possible, 
selecting to select only those that are necessary for the purpose.

#### R59 - Using trusted package repositories

Only repositories that are internal to the organization or official (from the distribution or a publisher) should be used.

#### R60 - Use hardened package repositories

When the distribution provides several types of repositories, preference should be given to those containing packages 
that are subject to additional hardening measures. Between two packages providing the same service, the ones that are 
hardened (at compile-time, at install time or in the default configuration) should be preferred. configuration) 
should be preferred.

```bash
root@debian:/home/debian# cat /etc/apt/sources.list
[...]
deb http://security.debian.org/debian-security bullseye-security main
deb-src http://security.debian.org/debian-security bullseye-security main
deb http://deb.debian.org/debian bullseye main
deb-src http://deb.debian.org/debian bullseye main
[...]
```

#### R61 - Perform regular updates

It is recommended to have a regular and reactive security update procedure. 

```bash
root@debian:/home/debian# apt-get update && apt-get upgrade
```