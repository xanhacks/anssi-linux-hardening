Security-Enhanced Linux, abbreviated to SELinux, is an LSM widely used in Red Hat-derived distributions derived from Red Hat.

SELinux offers the following three modes of operation:

1. **disabled** mode in which SELinux rules are not loaded and no access messages are logged;
2. **permissive** mode in which SELinux rules are loaded and queried but no access is denied, and access error messages are logged
3. **enforcing** mode in which accesses are conditioned on SELinux rules.

The SELinux policies available by default on the distributions are the following:

1. **minimum** : politique minimale qui ne confine qu’un nombre très restreint de  
services.
2. **targeted** or **default** policy partitioning each system service in a domain. Interactive users are not partitioned.
3. **mls** policy that includes the targeted policy and additionally proposes the use of hierarchical classification levels (sensitivity).

#### R46 - Enable SELinux with the targeted policy

It is recommended to activate SELinux in *enforcing* mode and to use the *targeted* policy. To enable it, you can edit the file located in `/etc/selinux/config`.

```
root@debian:~# grep ^SELINUX /etc/selinux/config
SELINUX=enforcing
SELINUXTYPE=targeted
```

To check the status of SELinux, you can use the `sestatus` command as below :

```
root@debian:~# sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33
```
