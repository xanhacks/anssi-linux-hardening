Special attention should be paid to:
- to files and directories containing secret items, such as passwords
- Files and directories containing secret items, such as passwords, password fingerprints, secret or private keys;
- to IPC (Inter-Process Communication) files named,
sockets or pipes, which allow different processes to establish communication channels
communication channels between them;
- Temporary storage directories to which everyone has access;
- Executables with special rights, such as setuid (or suid abbreviated as Set User ID)
or setgid (or sgid short for Set Group ID).
Appropriate rights will have to be rationally applied to each of these types.

### Sensitive files and directories

#### R50 - Restrict access rights to sensitive files and directories

Sensitive files and directories should only be readable by users with a strict need to know. 
with a strict need-to-know.

#### R51 - Change secrets and access rights at installation

All sensitive files and those that contribute to authentication mechanisms must be set up at system installation. 
If default secrets are preconfigured, then they must be replaced during, or just after, the system installation phase.

### Named IPC files, sockets or pipes

#### R52 - Restrict access to sockets and named pipes

Named sockets and pipes must be protected from access by a directory with appropriate rights. 
The rights of the named socket or pipe must also restrict access.

The following commands list all sockets and associated process information for local sockets:

```bash
root@debian:/home/debian# /usr/bin/ss -xp
Netid           State           Recv-Q           Send-Q                                    Local Address:Port                        Peer Address:Port            Process
u_str           ESTAB           0                0                                                     * 13558                                  * 0                users:(("sshd",pid=840,fd=6),("sshd",pid=815,fd=6))
u_str           ESTAB           0                0                                                     * 11861                                  * 11862            users:(("named",pid=419,fd=2),("named",pid=419,fd=1))
u_str           ESTAB           0                0                           /run/systemd/journal/stdout 11826                                  * 11825            users:(("systemd-journal",pid=260,fd=25),("systemd",pid=1,fd=71))
u_str           ESTAB           0                0                           /run/systemd/journal/stdout 11823                                  * 11822            users:(("systemd-journal",pid=260,fd=22),("systemd",pid=1,fd=70))
u_str           ESTAB           0                0                                                     * 11825                                  * 11826            users:(("freshclam",pid=413,fd=2),("freshclam",pid=413,fd=1))
u_str           ESTAB           0                0                                                     * 11803                                  * 12048            users:(("systemd-resolve",pid=362,fd=20))
u_str           ESTAB           0                0                           /run/systemd/journal/stdout 11533                                  * 11532            users:(("systemd-journal",pid=260,fd=24),("systemd",pid=1,fd=60))
u_str           ESTAB           0                0                           /run/dbus/system_bus_socket 13737                                  * 13736            users:(("dbus-daemon",pid=414,fd=17))
u_str           ESTAB           0                0                           /run/systemd/journal/stdout 12062                                  * 12061            users:(("systemd-journal",pid=260,fd=27),("systemd",pid=1,fd=68))
u_str           ESTAB           0                0                                                     * 11804                                  * 12049            users:(("systemd-timesyn",pid=363,fd=15))
u_str           ESTAB           0                0                           /run/systemd/journal/stdout 11862                                  * 11861            users:(("systemd-journal",pid=260,fd=26),("systemd",pid=1,fd=73))
u_str           ESTAB           0                0                           /run/dbus/system_bus_socket 12048                                  * 11803            users:(("dbus-daemon",pid=414,fd=10))
u_str           ESTAB           0                0                                                     * 11890                                  * 11891            users:(("systemd-logind",pid=423,fd=2),("systemd-logind",pid=423,fd=1))
u_str           ESTAB           0                0                                                     * 11814                                  * 12050            users:(("systemd",pid=1,fd=62))
u_str           ESTAB           0                0                                                     * 13736                                  * 13737            users:(("systemd",pid=818,fd=21))
u_str           ESTAB           0                0                           /run/dbus/system_bus_socket 12050                                  * 11814            users:(("dbus-daemon",pid=414,fd=12))
[...]
```

The following command lists the IPC resource usage information:

```bash
root@debian:/home/debian# /usr/bin/ipcs

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages

------ Shared Memory Segments --------
key        shmid      owner      perms      bytes      nattch     status

------ Semaphore Arrays --------
key        semid      owner      perms      nsems
```

The following command lists all the virtual memories shared by the processes processes and their associated access rights:

```bash
root@debian:/home/debian# ls /dev/shm/
[...]
```

The following command provides detailed information on I/O and IPC for the system processes:

```bash
root@debian:/home/debian# /usr/bin/lsof
COMMAND    PID TID TASKCMD               USER   FD      TYPE             DEVICE SIZE/OFF       NODE NAME
systemd      1                           root  cwd       DIR              254,1     4096          2 /
systemd      1                           root  rtd       DIR              254,1     4096          2 /
systemd      1                           root  txt       REG              254,1  1739200     134175 /usr/lib/systemd/systemd
systemd      1                           root  mem       REG              254,1   457949     273845 /etc/selinux/default/contexts/files/file_contexts.homedirs.bin
systemd      1                           root  mem       REG              254,1  7283668     273843 /etc/selinux/default/contexts/files/file_contexts.bin
systemd      1                           root  mem       REG              254,1   149576     130578 /usr/lib/x86_64-linux-gnu/libgpg-error.so.0.29.0
systemd      1                           root  mem       REG              254,1  3076992     129582 /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1
systemd      1                           root  mem       REG              254,1    26984     130452 /usr/lib/x86_64-linux-gnu/libcap-ng.so.0.0.0
systemd      1                           root  mem       REG              254,1   617128     131836 /usr/lib/x86_64-linux-gnu/libpcre2-8.so.0.10.1
systemd      1                           root  mem       REG              254,1   149520     130293 /usr/lib/x86_64-linux-gnu/libpthread-2.31.so
systemd      1                           root  mem       REG              254,1    18688     130279 /usr/lib/x86_64-linux-gnu/libdl-2.31.so
systemd      1                           root  mem       REG              254,1   158400     129936 /usr/lib/x86_64-linux-gnu/liblzma.so.5.2.5
systemd      1                           root  mem       REG              254,1   890800     133030 /usr/lib/x86_64-linux-gnu/libzstd.so.1.4.8
systemd      1                           root  mem       REG              254,1   137568     130615 /usr/lib/x86_64-linux-gnu/liblz4.so.1.9.3
systemd      1                           root  mem       REG              254,1    35280     132182 /usr/lib/x86_64-linux-gnu/libip4tc.so.2.0.0
[...]
```

### Access rights

#### R53 - Avoid files or directories without a known user or without a known group

Files or directories without a user or group identifiable by the system must be analyzed and possibly corrected to have 
an owner or a group known by the known to the system.

```bash
root@debian:/home/debian#  find / -type f \( -nouser -o -nogroup \) -ls 2>/dev/null
   139403     24 -rwxr-sr-x   1 root     8           23040 févr.  4  2021 /usr/bin/dotlockfile
     3165     36 -rw-rw----   1 debian   8           36607 nov. 24 15:10 /var/mail/debian
```

#### R54 - Enable sticky bit on writable directories

All directories that are writable by everyone must have the sticky bit set.

the following command allows to list all the directories that can be modified by everyone and without sticky bit :

```bash
root@debian:/home/debian#  find / -type d \( -perm -0002 -a \! -perm -1000 \) -ls 2>/dev/null
[...]
```

The following command lists all the directories that can be modified by and whose owner is not root :

```bash
root@debian:/home/debian# find / -type d -perm -0002 -a \! -uid 0 -ls 2>/dev/null
[...]
```

#### R55 - Separate temporary directories of users

Each user or application must have its own temporary directory and must have have it exclusively.

#### R56 - Avoid the use of executables with special rights setuid and setgid

The following command allows to list all files with special setuid and setgid rights on the system:

```bash
root@debian:/home/debian# find / -type f -perm /6000 -ls 2>/dev/null
   144516   1332 -rwsr-xr-x   1 root     root      1360680 juil. 13  2021 /usr/sbin/exim4
   129628     40 -rwxr-sr-x   1 root     shadow      38912 août 26  2021 /usr/sbin/unix_chkpwd
   129653     88 -rwsr-xr-x   1 root     root        88304 févr.  7  2020 /usr/bin/gpasswd
   129652     32 -rwxr-sr-x   1 root     shadow      31160 févr.  7  2020 /usr/bin/expiry
   133245     44 -rwsr-xr-x   1 root     root        44632 févr.  7  2020 /usr/bin/newgrp
   144187    180 -rwsr-x---   1 root     sudogrp    182600 févr. 27  2021 /usr/bin/sudo
   129654     64 -rwsr-xr-x   1 root     root        63960 févr.  7  2020 /usr/bin/passwd
   142574    348 -rwxr-sr-x   1 root     ssh        354440 juil.  2 00:37 /usr/bin/ssh-agent
   131174     36 -rwxr-sr-x   1 root     tty         35048 janv. 20  2022 /usr/bin/wall
   133783     36 -rwsr-xr-x   1 root     root        35040 janv. 20  2022 /usr/bin/umount
   139403     24 -rwxr-sr-x   1 root     8           23040 févr.  4  2021 /usr/bin/dotlockfile
   129649     80 -rwxr-sr-x   1 root     shadow      80256 févr.  7  2020 /usr/bin/chage
   129651     52 -rwsr-xr-x   1 root     root        52880 févr.  7  2020 /usr/bin/chsh
   144935     16 -rwxr-sr-x   1 root     root        15208 nov. 19  2020 /usr/bin/dotlock.mailutils
   133781     56 -rwsr-xr-x   1 root     root        55528 janv. 20  2022 /usr/bin/mount
   133411     72 -rwsr-xr-x   1 root     root        71912 janv. 20  2022 /usr/bin/su
   135148     44 -rwxr-sr-x   1 root     crontab     43568 févr. 22  2021 /usr/bin/crontab
   129650     60 -rwsr-xr-x   1 root     root        58416 févr.  7  2020 /usr/bin/chfn
   137390     52 -rwsr-xr--   1 root     messagebus    51336 févr. 21  2021 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
   142581    472 -rwsr-xr-x   1 root     root         481608 juil.  2 00:37 /usr/lib/openssh/ssh-keysign
```

The following command removes the special rights setuid or setgid:

```bash
root@debian:/home/debian# chmod u-s <file>
root@debian:/home/debian# chmod g-s <file>
```

Example :
```bash
root@debian:/home/debian# chmod u-s /usr/bin/dotlockfile
root@debian:/home/debian# chmod g-s /usr/bin/dotlockfile
```

#### R57 - Avoid using executables with the special rights `setuid root` and `setgid root`

Executables with the special rights setuid root and setgid root should be kept to a minimum. 
When only administrators are expected to run them, these special rights (setuid or setgid) should be removed and 
commands like su or sudo, which can be monitored, should be used instead.

The following command allows to list all files with special setuid root rights on the system:

```bash
root@debian:/home/debian# find / -user root -perm /2000 -ls 2>/dev/null
   129628     40 -rwxr-sr-x   1 root     shadow      38912 août 26  2021 /usr/sbin/unix_chkpwd
   150045      4 drwxrwsr-x   2 root     staff        4096 sept. 20 09:59 /usr/local/share/fonts
   129652     32 -rwxr-sr-x   1 root     shadow      31160 févr.  7  2020 /usr/bin/expiry
   142574    348 -rwxr-sr-x   1 root     ssh        354440 juil.  2 00:37 /usr/bin/ssh-agent
   131174     36 -rwxr-sr-x   1 root     tty         35048 janv. 20  2022 /usr/bin/wall
   129649     80 -rwxr-sr-x   1 root     shadow      80256 févr.  7  2020 /usr/bin/chage
   144935     16 -rwxr-sr-x   1 root     root        15208 nov. 19  2020 /usr/bin/dotlock.mailutils
   135148     44 -rwxr-sr-x   1 root     crontab     43568 févr. 22  2021 /usr/bin/crontab
      206      4 drwxrwsr-x   2 root     staff        4096 sept.  3 14:10 /var/local
      799      8 drwxr-sr-x   3 root     systemd-journal     4096 sept. 16 16:59 /var/log/journal
     3086      8 drwxr-sr-x   2 root     systemd-journal     4096 nov. 24 11:29 /var/log/journal/e9d10e680667494d98a2db0f75fc8eed
      255      4 drwxrwsr-x   2 root     8                   4096 nov. 24 15:10 /var/mail
      400      0 drwxr-sr-x   2 root     systemd-journal       40 nov. 24 14:51 /run/log/journal
   150309      4 drwxr-sr-x   3 root     bind                4096 sept. 22 10:35 /etc/bind
   269110      4 drwxr-sr-x   2 root     bind                4096 sept. 22 11:15 /etc/bind/zones
```

The following command allows to list all files with special setgid root rights on the system:

```bash
root@debian:/home/debian# find / -user root -perm /4000 -ls 2>/dev/null
   144516   1332 -rwsr-xr-x   1 root     root      1360680 juil. 13  2021 /usr/sbin/exim4
   129653     88 -rwsr-xr-x   1 root     root        88304 févr.  7  2020 /usr/bin/gpasswd
   133245     44 -rwsr-xr-x   1 root     root        44632 févr.  7  2020 /usr/bin/newgrp
   144187    180 -rwsr-x---   1 root     sudogrp    182600 févr. 27  2021 /usr/bin/sudo
   129654     64 -rwsr-xr-x   1 root     root        63960 févr.  7  2020 /usr/bin/passwd
   133783     36 -rwsr-xr-x   1 root     root        35040 janv. 20  2022 /usr/bin/umount
   129651     52 -rwsr-xr-x   1 root     root        52880 févr.  7  2020 /usr/bin/chsh
   133781     56 -rwsr-xr-x   1 root     root        55528 janv. 20  2022 /usr/bin/mount
   133411     72 -rwsr-xr-x   1 root     root        71912 janv. 20  2022 /usr/bin/su
   129650     60 -rwsr-xr-x   1 root     root        58416 févr.  7  2020 /usr/bin/chfn
   137390     52 -rwsr-xr--   1 root     messagebus    51336 févr. 21  2021 /usr/lib/dbus-1.0/dbus-daemon-launch-helper
   142581    472 -rwsr-xr-x   1 root     root         481608 juil.  2 00:37 /usr/lib/openssh/ssh-keysign
```


