#### R5 - Set up a password for the bootloader

A boot loader that allows you to protect its startup with a password is preferable. 
The password should prevent any user from changing its configuration options.
When the boot loader does not offer the possibility to add a password, 
another technical or organizational measure must be put in place to block 
any attempt to modify the configuration options by an unauthorized user.

Example : LVM Encryption
```
sda5_crypt : Gn1UDgaX0LlRmc5XIYw8
```

#### R6 - Protect the command line parameters of the kernel and `initramfs`

It is recommended to protect the Linux kernel command line parameters kernel command line parameters and `initramfs` to be checked during UEFI secure boot.

### Memory configuration

#### R7 - Activate IOMMU

It is recommended to enable IOMMU by adding the `iommu=force` directive to 
the list of kernel parameters at boot time in addition to those already present in the boot loader configuration files.

#### R8 - Set the memory configuration options

The memory configuration options detailed in this list are to be added to the list of kernel parameters to the list 
of kernel parameters at boot time in addition to those already present in the boot loader configuration file:

- `l1tf=full,force` : enables without the possibility of later disabling all countermeasures for the 
L1 Terminal Fault (L1TF) vulnerability present on most Intel processors (in 2018 at least). Note that this disables
Symmetric MultiThreading (SMT) and thus can have a strong impact on system performance.
However, this option is only necessary when the system is likely to be used as a hypervisor. If the virtual machines
are trusted, i.e. with a guest operating system that is both trusted and protected against the L1TF vulnerability, 
this option is not necessary and can even be replaced by `l1tf=off` to maximize performance

- `page_poison=on` : enable page poisoning of the page allocator (buddy allocator). This feature allows to fill the 
released pages with patterns when they are released and to check the patterns when they are allocated. This padding 
reduces the risk of information leakage from the released data

- `pti=on` : forces the use of Page Table Isolation (PTI) even on processors claiming not to be impacted by the Meltdown vulnerability;

- `slab_nomerge=yes` : disables merging of slab caches (dynamic memory allocations) of the same size. 
This feature allows to differentiate allocations between different slab caches, and greatly complicates heap 
massaging methodologies in case of heap overflow;

- `slub_debug=FZP`: enables some options for checking slab caches (dynamic memory allocations):
  - F enables consistency testing of slab cache metadata,
  - Z activates Red Zoning; in a slab cache, adds a red zone after each object to detect writes after it. It is important to note that the value used for the red zone is not random and is therefore a much weaker hardening than using real canaries,
  - P activates the poisoning of objects and padding, i.e. causes an error when accessing the poisoned areas;

- `spec_store_bypass_disable=seccomp` : forces the system to use the default countermeasure (on an x86 system supporting seccomp) for the Spectre v4 vulnerability (Speculative Store Bypass);

- `spectre_v2=on` : forces the system to use a countermeasure for the Spectre v2 vulnerability (Branch Target Injection). This option enables spectre_v2_user=on which prevents Single Threaded Indirect Branch Predictors (STIBP) and Indirect Branch Prediction Barrier 12(IBPB) attacks;

- `mds=full,nosmt` : forces the system to use Microarchitectural Data Sampling (MDS) to mitigate vulnerabilities in Intel processors. The mds=full option, which leaves Symmetric MultiThreading (SMT) enabled, is therefore not a complete mitigation. This mitigation requires an Intel firmware update and also mitigates the Intel processor TSX Asynchronous Abort (TAA) vulnerability on systems affected by MDS;

- `mce=0` : forces a kernel panic on uncorrected errors reported by Machine Check support. Otherwise, some of them only cause a SIGBUS to be sent, potentially allowing a malicious process to continue trying to exploit a vulnerability e.g. Rowhammer;

- `page_alloc.shuffle=1` : enables page allocator randomization which improves performance for direct-mapped memory-side-cache usage but reduces predictability of page allocations and thus complements SLAB_FREELIST_RANDOM;

- `rng_core.default_quality=500` : increases confidence in HWRNG of the TPM for robust and fast initialization of the Linux CSPRNG by crediting the half

#### R9 - Set the kernel configuration options

The kernel configuration options detailed in this list are presented as encountered in the as encountered in the sysctl.conf configuration file:

```bash
root@debian:/home/debian# vi /etc/sysctl.conf
[...]
kernel.dmesg_restrict=1
kernel.kptr_restrict=2
kernel.pid_max=65536
kernel.perf_cpu_time_max_percent=1
kernel.perf_event_max_sample_rate=1
kernel.perf_event_paranoid=2
kernel.randomize_va_space=2
kernel.sysrq=0
kernel.unprivileged_bpf_disabled=1
kernel.panic_on_oops=1
[...]
```

#### R10 - Disable kernel module loading

It is recommended to block the loading of kernel modules by enabling the `kernel.modules_disabled` kernel configuration option.

The kernel configuration option to block the loading of kernel modules is presented as found in the configuration file  `/etc/sysctl.conf` :

```bash
root@debian:/home/debian# vi /etc/sysctl.conf
[...]
kernel.modules_disabled=1
[...]
```

###  Configuration des processus

#### R11 - Activate and configure the Yama LSM

It is recommended to load the Yama security module at boot time, for example by passing the `security=yama`
directive to the kernel, and to set the `kernel.yama.ptrace_scope` kernel configuration option a value of at least 1.

```bash
root@debian:/home/debian# vi /etc/sysctl.conf
[...]
kernel.yama.ptrace_scope=1
[...]
```

### R12 - Set the network configuration options IPv4

The list below shows the IPv4 network configuration options for a typical non-routing "server" host 
"host that does not perform routing and has a minimalist IPv4 configuration (static configuration (static addressing).

The IPv4 network configuration options detailed in this list are recommended for a "server" type host not performing 
routing and having a minimalist IPv4 configuration. They are presented as found in the configuration file `sysctl.conf` :

```bash
root@debian:/home/debian# vi /etc/sysctl.conf
[...]
net.core.bpf_jit_harden=2
net.ipv4.ip_forward=0
net.ipv4.conf.all.accept_local=0
net.ipv4.conf.all.accept_redirects=0
net.ipv4.conf.default.accept_redirects=0
net.ipv4.conf.all.secure_redirects=0
net.ipv4.conf.default.secure_redirects=0
net.ipv4.conf.all.shared_media=0
net.ipv4.conf.default.shared_media=0
net.ipv4.conf.all.accept_source_route=0
net.ipv4.conf.default.accept_source_route=0
net.ipv4.conf.all.arp_filter=1
net.ipv4.conf.all.arp_ignore=2
net.ipv4.conf.all.route_localnet=0
net.ipv4.conf.all.drop_gratuitous_arp=1
net.ipv4.conf.default.rp_filter=1
net.ipv4.conf.all.rp_filter=1
net.ipv4.conf.default.send_redirects=0
net.ipv4.conf.all.send_redirects=0
net.ipv4.icmp_ignore_bogus_error_responses=1
net.ipv4.ip_local_port_range=32768 65535
net.ipv4.tcp_rfc1337=1
net.ipv4.tcp_syncookies=1
[...]
```

#### R13 - Disable IPv6 plan

When IPv6 is not used, it is recommended to disable the IPv6 stack by :
- adding the `ipv6.disable=1` directive to the list of kernel parameters chosen at boot time in addition to those 
already present in the boot loader configuration files
- enabling the network configuration options shown in the list below.

```bash
root@debian:/home/debian# vi /etc/sysctl.conf
[...]
net.ipv6.conf.default.disable_ipv6=1
net.ipv6.conf.all.disable_ipv6=1
[...]
```

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
GRUB_CMDLINE_LINUX=" ipv6.disable=1"
[...]
```


