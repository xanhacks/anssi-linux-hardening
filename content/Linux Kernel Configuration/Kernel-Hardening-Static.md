###  Hardware target independent configuration

#### R15 - Set compiler options for memory management 

The list below shows the recommended kernel compiler options for harden memory management.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_STRICT_KERNEL_RWX=y
CONFIG_ARCH_OPTIONAL_KERNEL_RWX=y
CONFIG_ARCH_HAS_STRICT_KERNEL_RWX=y
CONFIG_DEBUG_WX=y
CONFIG_DEBUG_FS=n
CONFIG_STACKPROTECTOR=y
CONFIG_STACKPROTECTOR_STRONG=y
CONFIG_SCHED_STACK_END_CHECK=y
CONFIG_HARDENED_USERCOPY=y
CONFIG_VMAP_STACK=y
CONFIG_REFCOUNT_FULL=y
CONFIG_FORTIFY_SOURCE=y
CONFIG_SECURITY_DMESG_RESTRICT=y
CONFIG_RETPOLINE=y
CONFIG_LEGACY_VSYSCALL_NONE=y
CONFIG_LEGACY_VSYSCALL_EMULATE=n
CONFIG_LEGACY_VSYSCALL_XONLY=n
CONFIG_X86_VSYSCALL_EMULATION=n
[...]
```

- `STRICT_KERNEL_RWX` : [see documentation](https://cateee.net/lkddb/web-lkddb/STRICT_KERNEL_RWX.html)
- `ARCH_OPTIONAL_KERNEL_RWX` : [see documentation](https://cateee.net/lkddb/web-lkddb/ARCH_OPTIONAL_KERNEL_RWX.html)
- `ARCH_HAS_STRICT_KERNEL_RWX` : [see documentation](https://cateee.net/lkddb/web-lkddb/ARCH_HAS_STRICT_KERNEL_RWX.html)
- `DEBUG_WX` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEBUG_WX.html)
- `DEBUG_FS` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEBUG_FS.html)
- `STACKPROTECTOR` : [see documentation](https://cateee.net/lkddb/web-lkddb/STACKPROTECTOR.html)
- `STACKPROTECTOR_STRONG` : [see documentation](https://cateee.net/lkddb/web-lkddb/STACKPROTECTOR_STRONG.html)
- `SCHED_STACK_END_CHECK` : [see documentation](https://cateee.net/lkddb/web-lkddb/SCHED_STACK_END_CHECK.html)
- `HARDENED_USERCOPY` : [see documentation](https://cateee.net/lkddb/web-lkddb/HARDENED_USERCOPY.html)
- `VMAP_STACK` : [see documentation](https://cateee.net/lkddb/web-lkddb/VMAP_STACK.html)
- `REFCOUNT_FULL` : [see documentation](https://cateee.net/lkddb/web-lkddb/REFCOUNT_FULL.html)
- `FORTIFY_SOURCE` : [see documentation](https://cateee.net/lkddb/web-lkddb/FORTIFY_SOURCE.html)
- `SECURITY_DMESG_RESTRICT` : [see documentation](https://cateee.net/lkddb/web-lkddb/SECURITY_DMESG_RESTRICT.html)
- `RETPOLINE` : [see documentation](https://cateee.net/lkddb/web-lkddb/RETPOLINE.html)
- `LEGACY_VSYSCALL_NONE` : [see documentation](https://cateee.net/lkddb/web-lkddb/LEGACY_VSYSCALL_NONE.html)
- `LEGACY_VSYSCALL_EMULATE` : [see documentation](https://cateee.net/lkddb/web-lkddb/LEGACY_VSYSCALL_EMULATE.html)
- `LEGACY_VSYSCALL_XONLY` : [see documentation](https://cateee.net/lkddb/web-lkddb/LEGACY_VSYSCALL_XONLY.html)
- `X86_VSYSCALL_EMULATION` : [see documentation](https://cateee.net/lkddb/web-lkddb/X86_VSYSCALL_EMULATION.html)

#### R16 - Set compilation options for kernel data structures

The list below shows the recommended kernel compiler options to protect the kernel data structures :

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_DEBUG_CREDENTIALS=y
CONFIG_DEBUG_NOTIFIERS=y
CONFIG_DEBUG_LIST=y
CONFIG_DEBUG_SG=y
CONFIG_BUG_ON_DATA_CORRUPTION=y
[...]
```

- `DEBUG_CREDENTIALS` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEBUG_CREDENTIALS.html)
- `DEBUG_NOTIFIERS` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEBUG_NOTIFIERS.html)
- `DEBUG_LIST` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEBUG_LIST.html)
- `DEBUG_SG` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEBUG_SG.html)
- `BUG_ON_DATA_CORRUPTION` : [see documentation](https://cateee.net/lkddb/web-lkddb/BUG_ON_DATA_CORRUPTION.html)


#### R17 - Set compiler options for the memory allocator

The list below shows the recommended kernel compiler options for harden the memory allocator :

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_SLAB_FREELIST_RANDOM=y
CONFIG_SLUB=y
CONFIG_SLAB_FREELIST_HARDENED=y
CONFIG_SLAB_MERGE_DEFAULT=n
CONFIG_SLUB_DEBUG=y
CONFIG_PAGE_POISONING=y
CONFIG_PAGE_POISONING_NO_SANITY=y
CONFIG_PAGE_POISONING_ZERO=y
[...]
```

- `SLAB_FREELIST_RANDOM` : [see documentation](https://cateee.net/lkddb/web-lkddb/SLAB_FREELIST_RANDOM.html)
- `SLUB` : [see documentation](https://cateee.net/lkddb/web-lkddb/SLUB.html)
- `SLAB_FREELIST_HARDENED` : [see documentation](https://cateee.net/lkddb/web-lkddb/SLAB_FREELIST_HARDENED.html)
- `SLAB_MERGE_DEFAULT` : [see documentation](https://cateee.net/lkddb/web-lkddb/SLAB_MERGE_DEFAULT.html)
- `SLUB_DEBUG` : [see documentation](https://cateee.net/lkddb/web-lkddb/SLUB_DEBUG.html)
- `PAGE_POISONING` : [see documentation](https://cateee.net/lkddb/web-lkddb/PAGE_POISONING.html)
- `PAGE_POISONING_NO_SANITY` : [see documentation](https://cateee.net/lkddb/web-lkddb/PAGE_POISONING_NO_SANITY.html)
- `PAGE_POISONING_ZERO` : [see documentation](https://cateee.net/lkddb/web-lkddb/PAGE_POISONING_ZERO.html)

#### R18 - Set the compiler options for the management of kernel modules

The list below shows the recommended kernel compilation options for harden the handling of kernel modules :

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_MODULES=y
CONFIG_STRICT_MODULE_RWX=y
CONFIG_MODULE_SIG=y
CONFIG_MODULE_SIG_FORCE=y
CONFIG_MODULE_SIG_ALL=y
CONFIG_MODULE_SIG_SHA512=y
CONFIG_MODULE_SIG_HASH="sha512"
CONFIG_MODULE_SIG_KEY="certs/signing_key.pem"
[...]
```

- `MODULES` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULES.html)
- `STRICT_MODULE_RWX` : [see documentation](https://cateee.net/lkddb/web-lkddb/STRICT_MODULE_RWX.html)
- `MODULE_SIG` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULE_SIG.html)
- `MODULE_SIG_FORCE` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULE_SIG_FORCE.html)
- `MODULE_SIG_ALL` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULE_SIG_ALL.html)
- `MODULE_SIG_SHA512` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULE_SIG_SHA512.html)
- `MODULE_SIG_HASH` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULE_SIG_HASH.html)
- `MODULE_SIG_KEY` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULE_SIG_KEY.html)

#### R19 - Set compilation options for abnormal events

The list below shows the recommended kernel compiler options for react to abnormal events.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_BUG=y
CONFIG_PANIC_ON_OOPS=y
CONFIG_PANIC_TIMEOUT=-1
[...]
```

- `BUG` : [see documentation](https://cateee.net/lkddb/web-lkddb/BUG.html)
- `PANIC_ON_OOPS` : [see documentation](https://cateee.net/lkddb/web-lkddb/PANIC_ON_OOPS.html)
- `PANIC_TIMEOUT` : [see documentation](https://cateee.net/lkddb/web-lkddb/PANIC_TIMEOUT.html)

#### R20 - Set compilation options for kernel security primitives

The following is a list of recommended kernel compilation options for configuring kernel security primitives.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_SECCOMP=y
CONFIG_SECCOMP_FILTER=y
CONFIG_SECURITY=y
CONFIG_SECURITY_YAMA=y
#CONFIG_SECURITY_WRITABLE_HOOKS is not set
[...]
```

- `SECCOMP` : [see documentation](https://cateee.net/lkddb/web-lkddb/SECCOMP.html)
- `SECCOMP_FILTER` : [see documentation](https://cateee.net/lkddb/web-lkddb/SECCOMP_FILTER.html)
- `SECURITY` : [see documentation](https://cateee.net/lkddb/web-lkddb/SECURITY.html)
- `SECURITY_YAMA` : [see documentation](https://cateee.net/lkddb/web-lkddb/SECURITY_YAMA.html)
- `SECURITY_WRITABLE_HOOKS` : [see documentation](https://cateee.net/lkddb/web-lkddb/SECURITY_WRITABLE_HOOKS.html)

#### R21 - Set the compiler options for the compiler plugins

The list below shows the recommended kernel compilation options for configuring the compiler plugins.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_GCC_PLUGINS=y
CONFIG_GCC_PLUGIN_LATENT_ENTROPY=y
CONFIG_GCC_PLUGIN_STACKLEAK=y
CONFIG_GCC_PLUGIN_STRUCTLEAK=y
CONFIG_GCC_PLUGIN_STRUCTLEAK_BYREF_ALL=y
CONFIG_GCC_PLUGIN_RANDSTRUCT=y
[...]
```

- `GCC_PLUGINS` : [see documentation](https://cateee.net/lkddb/web-lkddb/GCC_PLUGINS.html)
- `GCC_PLUGIN_LATENT_ENTROPY` : [see documentation](https://cateee.net/lkddb/web-lkddb/GCC_PLUGIN_LATENT_ENTROPY.html)
- `GCC_PLUGIN_STACKLEAK` : [see documentation](https://cateee.net/lkddb/web-lkddb/GCC_PLUGIN_STACKLEAK.html)
- `GCC_PLUGIN_STRUCTLEAK` : [see documentation](https://cateee.net/lkddb/web-lkddb/GCC_PLUGIN_STRUCTLEAK.html)
- `GCC_PLUGIN_STRUCTLEAK_BYREF_ALL` : [see documentation](https://cateee.net/lkddb/web-lkddb/GCC_PLUGIN_STRUCTLEAK_BYREF_ALL.html)
- `GCC_PLUGIN_RANDSTRUCT` : [see documentation](https://cateee.net/lkddb/web-lkddb/GCC_PLUGIN_RANDSTRUCT.html)

#### R22 - Set the compile options for the network

The list below shows the recommended kernel build options for configuring the network stack.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
#CONFIG_IPV6 option set.
CONFIG_SYN_COOKIES=y
[...]
```

- `IPV6` : [see documentation](https://cateee.net/lkddb/web-lkddb/IPV6.html)
- `SYN_COOKIES` : [see documentation](https://cateee.net/lkddb/web-lkddb/SYN_COOKIES.html)

#### R23 - Set compiler options for various kernel behaviors

The list below shows the recommended kernel compiler options for various kernel behaviors.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
#CONFIG_KEXEC is not set
#CONFIG_HIBERNATION is not set
#CONFIG_BINFMT_MISC is not set
#CONFIG_LEGACY_PTYS is not set
#CONFIG_MODULES is not set
[...]
```
- `KEXEC` : [see documentation](https://cateee.net/lkddb/web-lkddb/KEXEC.html)
- `HIBERNATION` : [see documentation](https://cateee.net/lkddb/web-lkddb/HIBERNATION.html)
- `BINFMT_MISC` : [see documentation](https://cateee.net/lkddb/web-lkddb/BINFMT_MISC.html)
- `LEGACY_PTYS` : [see documentation](https://cateee.net/lkddb/web-lkddb/LEGACY_PTYS.html)
- `MODULES` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODULES.html)


### Specific configuration for hardware architectures

#### R24 - Set specific compilation options for 32-bit architectures

The list below shows the recommended kernel compilation options for 32-bit x86 architectures. 
Not all of them are relevant on 64-bit architectures.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_HIGHMEM64G=y
CONFIG_X86_PAE=y
CONFIG_DEFAULT_MMAP_MIN_ADDR=65536
CONFIG_RANDOMIZE_BASE=y
[...]
```

- `HIGHMEM64G` : [see documentation](https://cateee.net/lkddb/web-lkddb/HIGHMEM64G.html)
- `X86_PAE` : [see documentation](https://cateee.net/lkddb/web-lkddb/X86_PAE.html)
- `DEFAULT_MMAP_MIN_ADDR` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEFAULT_MMAP_MIN_ADDR.html)
- `RANDOMIZE_BASE` : [see documentation](https://cateee.net/lkddb/web-lkddb/RANDOMIZE_BASE.html)

#### R25 - Set specific compilation options for x86_64 bit architectures

The list below shows the recommended kernel compilation options for x86_64-bit architectures. 
Not all of them are relevant for 32-bit architectures.

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_X86_64=y
CONFIG_DEFAULT_MMAP_MIN_ADDR=65536
CONFIG_RANDOMIZE_BASE=y
CONFIG_RANDOMIZE_MEMORY=y
CONFIG_PAGE_TABLE_ISOLATION=y
#CONFIG_IA32_EMULATION is not set
#CONFIG_MODIFY_LDT_SYSCALL is not set
[...]
```

- `X86_64` : [see documentation](https://cateee.net/lkddb/web-lkddb/X86_64.html)
- `DEFAULT_MMAP_MIN_ADDR` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEFAULT_MMAP_MIN_ADDR.html)
- `RANDOMIZE_BASE` : [see documentation](https://cateee.net/lkddb/web-lkddb/RANDOMIZE_BASE.html)
- `RANDOMIZE_MEMORY` : [see documentation](https://cateee.net/lkddb/web-lkddb/RANDOMIZE_MEMORY.html)
- `PAGE_TABLE_ISOLATION` : [see documentation](https://cateee.net/lkddb/web-lkddb/PAGE_TABLE_ISOLATION.html)
- `IA32_EMULATION` : [see documentation](https://cateee.net/lkddb/web-lkddb/IA32_EMULATION.html)
- `MODIFY_LDT_SYSCALL` : [see documentation](https://cateee.net/lkddb/web-lkddb/MODIFY_LDT_SYSCALL.html)

#### R26 - Set specific compilation options for ARM architectures

The list below shows the recommended kernel compilation options for ARM-type architectures

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_DEFAULT_MMAP_MIN_ADDR=32768
CONFIG_VMSPLIT_3G=y
CONFIG_STRICT_MEMORY_RWX=y
CONFIG_CPU_SW_DOMAIN_PAN=y
#CONFIG_OABI_COMPAT is unset
[...]
```

- `DEFAULT_MMAP_MIN_ADDR` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEFAULT_MMAP_MIN_ADDR.html)
- `VMSPLIT_3G` : [see documentation](https://cateee.net/lkddb/web-lkddb/VMSPLIT_3G.html)
- `STRICT_MEMORY_RWX` : [see documentation](https://cateee.net/lkddb/web-lkddb/STRICT_MEMORY_RWX.html)
- `CPU_SW_DOMAIN_PAN` : [see documentation](https://cateee.net/lkddb/web-lkddb/CPU_SW_DOMAIN_PAN.html)
- `OABI_COMPAT` : [see documentation](https://cateee.net/lkddb/web-lkddb/OABI_COMPAT.html)

#### R27 - Set specific compilation options for for 64-bit ARM architectures

The list below shows the recommended kernel compilation options for 64-bit ARM architectures

```bash
root@debian:/home/debian# vi /etc/default/grub
[...]
CONFIG_DEFAULT_MMAP_MIN_ADDR=32768
CONFIG_RANDOMIZE_BASE=y
CONFIG_ARM64_SW_TTBR0_PAN=y
CONFIG_UNMAP_KERNEL_AT_EL0=y
[...]
```

- `DEFAULT_MMAP_MIN_ADDR` : [see documentation](https://cateee.net/lkddb/web-lkddb/DEFAULT_MMAP_MIN_ADDR.html)
- `RANDOMIZE_BASE` : [see documentation](https://cateee.net/lkddb/web-lkddb/RANDOMIZE_BASE.html)
- `ARM64_SW_TTBR0_PAN` : [see documentation](https://cateee.net/lkddb/web-lkddb/ARM64_SW_TTBR0_PAN.html)
- `UNMAP_KERNEL_AT_EL0` : [see documentation](https://cateee.net/lkddb/web-lkddb/UNMAP_KERNEL_AT_EL0.html)