---
title: Linux Kernel Configuration
---

> Hardening the Linux kernel consists of increasing the protection mechanisms of the operating system and validating the presence of the kernel's self-protection mechanisms, following the principle of defense in depth. It is possible to provide protection or countermeasures to potential software or hardware vulnerabilities of the platform and at the same time reduce the attack surface of the kernel, which is initially very large.

{{< toc-tree >}}