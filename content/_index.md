---
title: GNU/Linux Hardening
---

# Introduction

In this wiki we will document the recommendations for configuring a GNU/Linux system. In particular we will look at the general principles of security, hardware security, kernel security, services security, ... For this we will use the document provided by the ANSSI.

# Table of Contents

{{< toc-tree >}}

# References

- [Recommandations de sécurité relatives à un système GNU/Linux - v2. 0](https://www.ssi.gouv.fr/uploads/2019/02/fr_np_linux_configuration-v2.0.pdf)
