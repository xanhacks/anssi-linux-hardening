---
title: Hardware configuration
---

> This chapter deals with recommendations to harden the base that will be used for the installation. This setting should preferably be done before the installation so that the system is able to system is able to take them into account as soon as possible.


{{< toc-tree >}}