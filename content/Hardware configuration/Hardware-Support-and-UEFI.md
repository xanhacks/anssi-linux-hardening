![](https://www.passmoz.com/images/others/firmware-workflow.jpg)

#### R1 - Choose and configure your equipment

It is recommended to apply the recommendations of the material support mentioned
in this technical documentation (cf : [french documentation](https://www.ssi.gouv.fr/guide/recommandations-de-configuration-materielle-de-postes-clients-et-serveurs-x86/#:~:text=systèmes%20d%27information-,Recommandations%20de%20configuration%20matérielle%20de%20postes%20clients%20et%20serveurs%20x86,globalement%20la%20sécurité%20du%20système.))

#### R2 - Configuring the BIOS/UEFI

It is recommended to apply the recommendations of configuration of the BIOS/UEFI noted
in this technical documentation (cf : [french documentation](https://www.ssi.gouv.fr/guide/recommandations-de-configuration-materielle-de-postes-clients-et-serveurs-x86/#:~:text=systèmes%20d%27information-,Recommandations%20de%20configuration%20matérielle%20de%20postes%20clients%20et%20serveurs%20x86,globalement%20la%20sécurité%20du%20système.))

#### R3 - Enable UEFI secure boot

It is recommended to enable the UEFI secure boot configuration associated with
the distribution. We can do it by enabling SecureBoot in the BIOS.

#### R4 - Replace preloaded keys

It is recommended to replace the keys preloaded in the UEFI with new keys with which
keys with which to sign :
- the boot loader and the Linux kernel 

or

- the Linux kernel image in EFI format.

